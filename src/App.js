// eslint-disable-next-line
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Routes,
} from 'react-router-dom';
import WeatherApp from './components/weatherApp';



function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route exact path='/' element={<WeatherApp />}>
            {({ match }) => <WeatherApp match={match} />}
          </Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
