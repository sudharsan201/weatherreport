// eslint-disable-next-line

import React, { useState, useRef, useEffect } from "react";
import "./style.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import moment from 'moment';
import { Form, Button } from 'react-bootstrap'
import { AiFillQuestionCircle } from 'react-icons/ai';
import endpoints from '../hoc/endpoints';

export default function WeatherApp() {
    const [dataList, setDataList] = useState([]);
    const [name, setName] = useState('Visakhapatnam');
    const [loader, setLoader] = useState(false);
    const formRef = useRef();


    function getWeatherUpdates(name) {
        setLoader(true);
        let appId = '1635890035cbba097fd5c26c8ea672a1';
        axios.get(`${endpoints.getWeatherUpdates}?q=${name}&appid=${appId}`)
            .then((response) => {
                setLoader(false);
                setDataList(response.data);

            })
    }
    const submitHandler = (event) => {
        event.preventDefault();
        getWeatherUpdates(name);
    }
    useEffect(() => {
        getWeatherUpdates(name)
    }, [name])

    function getDate(value) {
        let newDate = value.split(' ')[0]
        let date = new Date(newDate);
        return [
            date.getDate(),
            date.getMonth() + 1,
            date.getFullYear(),
        ].join('/');

    }

    return (
        <div className="weather-app-div">
            <div className='container'>
                <div className="row">
                    <div className="col-md-4 col-xs-12">
                        <h1 style={{ color: '#ec6536', fontfamily: 'sans-serif' }}>Weather In Your City</h1>
                    </div>
                    <div className="col-md-4 col-xs-12">
                        <Form
                            onSubmit={submitHandler}
                            ref={formRef}
                            className='main'
                        >
                            <Form.Control
                                type='text'
                                placeholder='Search'
                                onChange={(event) => setName(event.target.value)}
                                className="weather-app-search"
                                margin='dense'
                                value={name}
                            />
                            <Button
                                className="weather-submit-button"
                                type='submit'
                            >
                                <AiFillQuestionCircle />
                                Search
                            </Button>
                        </Form>
                    </div>
                    <div className='col-md-4 col-xs-12'>
                        {loader &&
                            <div className="weather-spinner"></div>
                        }
                    </div>
                </div>

            </div>


            <div className="container">
                {loader}
                <br />
                <div className="row">
                    {dataList?.list?.map((we, index) => (
                        moment(we.dt_txt).format('LTS') === '12:00:00 AM' &&
                        <div className="col-sm" key={index}>
                            < table className="table table-bordered weather-app-table" >
                                <thead>
                                    <tr>
                                        <th colSpan="4" style={{ backgroundColor: "#ec6536" }}> Date:{getDate(we.dt_txt)}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr className="weather-table-row">
                                        <th colSpan="4">Temprature</th>
                                    </tr>
                                    <tr className="weather-table-row">
                                        <th colSpan="2">Min</th>
                                        <th colSpan="2">Max</th>
                                    </tr>
                                    <tr className="weather-table-row">
                                        <td colSpan="2">{we.main.temp_max}</td>
                                        <td colSpan="2">{we.main.temp_max}</td>
                                    </tr>
                                    <tr>
                                        <th colSpan="2">Pressure</th>
                                        <td colSpan="2">{we.main.pressure}</td>
                                    </tr>
                                    <tr>
                                        <th colSpan="2">Humadity</th>
                                        <td colSpan="2">{we.main.humidity}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
}